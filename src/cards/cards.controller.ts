import { Body, Controller, Get, Post } from '@nestjs/common';
import { CardsService } from './cards.service';
import { Card } from '../interfaces/card.interface';
import { SaveReponseDto } from './dto/save-reponse.dto';
import { FlagDto } from './dto/flag.dto';

@Controller('cards')
export class CardsController {
  constructor(private cardsService: CardsService){}
    @Get()
  async getCards(): Promise<Card[]> {
    const cards = await this.cardsService.getCards();
    return cards;
    }
  
    @Post('reset')
    async resetGame(@Body() flag: FlagDto) {
      if (flag.reset) {
        const result = await this.cardsService.resetGame();
        return result;
      } else {
        return {msg: 'rien a été fait', date: new Date().toISOString()}
      }
    }
  
  @Post()
  async saveReponse(@Body() saveReponsesDTO: SaveReponseDto) {
  
    const result = await this.cardsService.saveReponse(saveReponsesDTO);
    console.log('saveReponse result', result);
    return result;
  }
}
