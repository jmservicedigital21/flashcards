import { Reponse } from "src/interfaces/reponse.interface";

export class SaveReponseDto {
    reponses: Reponse[];
}