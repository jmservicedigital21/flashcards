import { Injectable } from '@nestjs/common';
import { Card } from '../interfaces/card.interface';
import { Reponse } from '../../src/interfaces/reponse.interface';
import { Message } from './../interfaces/message.interface';


@Injectable()
export class CardsService {
  reponses: Reponse[] = [];
  
  cards = [
    {id:1, question: 'quel est le langage de web?', reponse: 'javascript'},
    { id: 2, question: 'quel est le metier le plus cool?', reponse: 'programeur' },
    {id:3, question: 'c\'est quoi une interface', reponse: 'un contrat'},
    { id:4, question: 'A quoi sert push()', reponse: 'A pousser' }
    
  ];

  getCards(): Promise<Card[]> {
    return new Promise(resolve => {
      resolve(this.selectCardsUsedFailedToReponse());
    })
  }
  selectCardsUsedFailedToReponse() {
    if (this.reponses.length === 0) return this.cards;
    const cardsUserFailedToReponse = this.reponses.filter(c => c.isRight === false);
    return cardsUserFailedToReponse.map(c => c.card);
  }

  saveReponse(reponses): Promise<Message> {
    return new Promise(resolve => {
      this.reponses = reponses
      resolve({ msg: 'reponses sauvegardés', date: new Date().toISOString() })
    });
  }
  resetGame(): Promise<Message> {
    return new Promise(resolve => {
      this.reponses = [];
      resolve({ msg: 'new game', date: new Date().toISOString() });
    })
  }
}
