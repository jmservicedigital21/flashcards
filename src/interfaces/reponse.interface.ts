import { Card } from './card.interface';

export interface Reponse {
    card: Card;
    isRight: boolean;
}
